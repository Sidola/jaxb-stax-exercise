import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import stax.model.Destroyer;
import stax.model.Marker;
import stax.model.PlayerBoard;
import stax.model.Ship;
import stax.model.Submarine;

/**
 * DISCLAIMER:
 * 
 * While this will successfully read the XML file as expected, I have no 
 * idea if this is actually the way you're supposed to use StAX.
 * 
 * Mainly because it looks insane when compared to JAXB. But I guess if speed
 * and memory is more important than sanity, this would be the preferred option.
 * 
 * - Sid
 */

public class RunStAX {

	private static final String PATH = "./src/Ships.xml";
	
	public static void main(String[] args) {
		PlayerBoardParser staxParser = new PlayerBoardParser();
		PlayerBoard pb = staxParser.parseFile(PATH);
		
		prettyPrintBoard(pb);		
	}
	
	/**
	 * Pretty-print the board.
	 * 
	 * @param pb - PlayerBoard object.
	 */
	public static void prettyPrintBoard(PlayerBoard pb) {
		// Iterate over ships
		for (Ship ship : pb.getShipList()) {
			System.out.println("-------- Ship --------");

			System.out.println("Ship name: " + ship.getClassName());
			System.out.println("Direction: " + ship.getDirection());
			
			// Iterate over each coordinate for that ship
			for (Marker marker : ship.getCoordinates()) {
				System.out.println(marker.toString());
			}
			
			System.out.println("----------------------\n");
		}	
	}
	
	/**
	 * PlayerBoardParser Class
	 */
	public static class PlayerBoardParser {
	
		static final String PLAYERBOARD = "ya.practices.battleship.board.PlayerBoard";
		static final String SHIPS = "ships";
		static final String DESTROYER = "ya.practices.battleship.ships.Destroyer";
		static final String SUBMARINE = "ya.practices.battleship.ships.Submarine";
		static final String COORDINATES = "coordinates";
		static final String MARKER = "ya.practices.battleship.board.Marker";
		static final String X = "x";
		static final String Y = "y";
		static final String ATTACKED = "attacked";
		static final String HIT = "hit";
		static final String SYMBOL = "symbol";
		static final String DIRECTION = "direction";
		
		/**
		 * Parses a valid XML file.
		 * 
		 * @param filePath
		 * @return - Returns a PlayerBoard object.
		 */
		public PlayerBoard parseFile(String filePath) {
			
			PlayerBoard pb = null;
			ArrayList<Ship> shipList = new ArrayList<Ship>();
			ArrayList<Marker> coordList = new ArrayList<Marker>();
			Marker marker = null;
			String direction = null;
			
			XMLEventReader eventReader;

			try {
				
				eventReader = StaxUtils.getEventReader(filePath);
				
				while(eventReader.hasNext()) {
					XMLEvent event = eventReader.nextEvent();
					
					// StartElements begin
					if (event.isStartElement()) {
						String thisElement = StaxUtils.getStartElementName(event);
						
						if (thisElement.equals(PLAYERBOARD)) {
							pb = new PlayerBoard();
							continue;
						}
			
						if (thisElement.equals(DESTROYER)) {
							Ship ship = new Destroyer();
							shipList.add(ship);
							continue;
						}
						
						if (thisElement.equals(SUBMARINE)) {
							Ship ship = new Submarine();
							shipList.add(ship);
							continue;
						}
						
						if (thisElement.equals(MARKER)) { 
							marker = new Marker();
							continue;
						}
						
						if (thisElement.equals(X)) {
							marker.setX(StaxUtils.getText(eventReader));
							continue;
						}
						
						if (thisElement.equals(Y)) {
							marker.setY(StaxUtils.getText(eventReader));
							continue;
						}
						
						if (thisElement.equals(ATTACKED)) {
							marker.setAttacked(StaxUtils.getText(eventReader));
							continue;
						}
						
						if (thisElement.equals(HIT)) {
							marker.setHit(StaxUtils.getText(eventReader));
							continue;
						}
						
						if (thisElement.equals(SYMBOL)) {
							marker.setSymbol(StaxUtils.getText(eventReader));
							continue;
						}
						
						if (thisElement.equals(DIRECTION)) {
							direction = StaxUtils.getText(eventReader);
							continue;
						}
						
					} // StartElements ends
					
					// EndElements begin
 					if (event.isEndElement()) {
 						String thisElement = StaxUtils.getEndElementName(event);
 						
 						if (thisElement.equals(PLAYERBOARD)) {
 							pb.setShipList(shipList);					
 						}
 						
 						if (thisElement.equals(DESTROYER) || thisElement.equals(SUBMARINE)) { 							
 							Ship ship = shipList.get(shipList.size() - 1);
 							
 							ship.setCoordinates(coordList);
 							ship.setDirection(direction);

 							direction = null;
 							coordList = new ArrayList<Marker>();
 						}
 						
 						if (thisElement.equals(MARKER)) {
 							coordList.add(marker);
 							marker = null;
 						}
 						
 					} // EndElements ends
					
				} // While-loop ends
				
			} catch (FileNotFoundException e) { e.printStackTrace();
			} catch (XMLStreamException e) { e.printStackTrace(); }

			return pb;
		}
			
	} // End of PlayerBoardParser class

	/**
	 * StAX Helper methods
	 */
	private static class StaxUtils {

		/**
		 * Creates and returns an EventReader object.
		 * 
		 * @param filePath
		 * @return
		 * @throws FileNotFoundException 
		 * @throws XMLStreamException 
		 */
		public static XMLEventReader getEventReader(String filePath) throws FileNotFoundException, XMLStreamException {
			XMLEventReader eventReader = null;
							
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();				
			InputStream in = new FileInputStream(filePath);
			eventReader = inputFactory.createXMLEventReader(in, "UTF-8");

			return eventReader;
		}	
		
		/**
		 * Wrapper to get the name of an element.
		 * 
		 * @param event
		 * @return - Returns the name of an element.
		 */
		public static String getStartElementName(XMLEvent event) {
			return event.asStartElement().getName().getLocalPart();
		}
		
		/**
		 * Wrapper to get the name of an element.
		 * 
		 * @param event
		 * @return - Returns the name of an element.
		 */
		public static String getEndElementName(XMLEvent event) {
			return event.asEndElement().getName().getLocalPart();
		}
		
		/**
		 * Wrapper to get text out of an element.
		 * 
		 * @param eventReader
		 * @return - Returns the contents of the element.
		 */
		public static String getText(XMLEventReader eventReader) {
			String retVal = null;
			try {
				retVal = eventReader.nextEvent().asCharacters().getData();
			} catch (XMLStreamException e) { e.printStackTrace(); }
			return retVal;
		}
		
	} // End of StaxUtils class
}







