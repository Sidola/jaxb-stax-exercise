package jaxb.model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class Ship {
	
	@XmlElementWrapper(name = "coordinates")
	@XmlElement(name = "ya.practices.battleship.board.Marker")
	private ArrayList<Marker> coordinates;

	@XmlElement
	private String direction;
		
	// Getters & Setters
	public ArrayList<Marker> getCoordinates() { return coordinates; }
	public void setCoordinates(ArrayList<Marker> coordinates) { this.coordinates = coordinates; }
	
	public String getDirection() { return direction; }
	public void setDirection(String direction) { this.direction = direction; }
	
	public String getClassName() { return this.getClass().getSimpleName(); }
}
