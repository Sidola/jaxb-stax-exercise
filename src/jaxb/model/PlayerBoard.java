package jaxb.model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "ya.practices.battleship.board.PlayerBoard")
public class PlayerBoard {

	@XmlElementRef
	@XmlElementWrapper(name = "ships")
	private ArrayList<Ship> shipList;

	// Getters & Setters
	public ArrayList<Ship> getShipList() { return shipList; }
	public void setShipList(ArrayList<Ship> shipList) { this.shipList = shipList; }
}
