import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import jaxb.model.Destroyer;
import jaxb.model.Marker;
import jaxb.model.PlayerBoard;
import jaxb.model.Ship;
import jaxb.model.Submarine;


public class RunJAXB {

	private static final String PATH = "./src/Ships.xml";
	private static final String PATH_NEW = "./src/Ships_new.xml";
	
	public static void main(String[] args) {
		
		// Loads an XML file
		PlayerBoard pb = loadXML(PATH);
		
		// Print the board
		prettyPrintBoard(pb);

		// Create and add a new ship to the board
		Ship newShip = new Submarine();
		
		// Create coords
		ArrayList<Marker> coordList = new ArrayList<Marker>();
		coordList.add(new Marker("10", "10", "false", "false", "S"));
		newShip.setCoordinates(coordList);
		
		// Set direction
		newShip.setDirection("Vertical");
		
		// Add ship to list
		ArrayList<Ship> shipList = pb.getShipList();
		shipList.add(newShip);
		pb.setShipList(shipList);
		
		// Save the XML file
		System.out.println("------------------------------------");
		System.out.println("Saving new ship to file...\n------------------------------------\n");
		saveXML(pb, PATH_NEW);
		
		// Load & Print again
		pb = loadXML(PATH_NEW);
		prettyPrintBoard(pb);
		
	}
	
	/**
	 * Saves XML to a given file.
	 * 
	 * @param pb - PlayerBoard object to save.
	 * @param path - Path to file.
	 */
	public static void saveXML(PlayerBoard pb, String path) {
		JAXBContext context;
		
		try {
			
			context = JAXBContext.newInstance(PlayerBoard.class, Destroyer.class, Submarine.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			m.marshal(pb, new File(path));
			
		} catch (JAXBException e) { e.printStackTrace(); }
	}
	
	/**
	 * Loads XML from the given file.
	 * 
	 * @param path - Path to file.
	 * @return - Returns a PlayerBoard object
	 */
	public static PlayerBoard loadXML(String path) {
		PlayerBoard pb = null;
		JAXBContext context;
		
		try {
			
			context = JAXBContext.newInstance(PlayerBoard.class, Destroyer.class, Submarine.class);
			Unmarshaller um = context.createUnmarshaller();
			
			pb = (PlayerBoard) um.unmarshal(
					new InputStreamReader(new FileInputStream(path),
							Charset.forName("UTF-8")));
			
		} catch (JAXBException e) { e.printStackTrace();
		} catch (FileNotFoundException e) { e.printStackTrace(); }
		
		return pb;
	}
	
	/**
	 * Pretty-print the board.
	 * 
	 * @param pb - PlayerBoard object.
	 */
	public static void prettyPrintBoard(PlayerBoard pb) {
		// Iterate over ships
		for (Ship ship : pb.getShipList()) {
			System.out.println("-------- Ship --------");

			System.out.println("Ship name: " + ship.getClassName());
			System.out.println("Direction: " + ship.getDirection());
			
			// Iterate over each coordinate for that ship
			for (Marker marker : ship.getCoordinates()) {
				System.out.println(marker.toString());
			}
			
			System.out.println("----------------------\n");
		}	
	}
}
