package stax.model;

import java.util.ArrayList;

public abstract class Ship {
	
	private ArrayList<Marker> coordinates;

	private String direction;
		
	// Getters & Setters
	public ArrayList<Marker> getCoordinates() { return coordinates; }
	public void setCoordinates(ArrayList<Marker> coordinates) { this.coordinates = coordinates; }
	
	public String getDirection() { return direction; }
	public void setDirection(String direction) { this.direction = direction; }
	
	public String getClassName() { return this.getClass().getSimpleName(); }
}
