package stax.model;

import java.util.ArrayList;

public class PlayerBoard {

	private ArrayList<Ship> shipList;

	// Getters & Setters
	public ArrayList<Ship> getShipList() { return shipList; }
	public void setShipList(ArrayList<Ship> shipList) { this.shipList = shipList; }
}
