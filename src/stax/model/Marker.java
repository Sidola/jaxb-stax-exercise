package stax.model;

public class Marker {

    private String x, y, attacked, hit, symbol;

    // Default constructor
    public Marker() {}
    
    /**
     * Constructor, used to create manual markers.
     * 
     * @param x
     * @param y
     * @param attacked
     * @param hit
     * @param symbol
     */
    public Marker(String x, String y, String attacked, String hit, String symbol) {
    	this.x = x; this.y = y; this.attacked = attacked;
        this.hit = hit; this.symbol = symbol;
    }
    
    @Override
    public String toString() {
        return "Coordinates: x" + x + ", y" + y + ".   Attacked: " + attacked + ".   Hit: " + hit + ".   Symbol: " + symbol;
    }
    
    // Getters & Setters
    public String getX() { return x; }
    public void setX(String x) { this.x = x; }

    public String getY() { return y; }
    public void setY(String y) { this.y = y; }

    public String getAttacked() { return attacked; }
    public void setAttacked(String attacked) { this.attacked = attacked; }

    public String getHit() { return hit; }
    public void setHit(String hit) { this.hit = hit; }

    public String getSymbol() { return symbol; }
    public void setSymbol(String symbol) { this.symbol = symbol; }
} 